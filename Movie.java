public class Movie {
    private String title;
    private String studio;
    private String rating;
    static int pgMoviesCount =0;

    Movie(String title,String studio,String rating){
        this.title=title;
        this.studio=studio;
        this.rating=rating;
    }

    Movie(String title, String studio) {
        this.title=title;
        this.studio=studio;
        this.rating="PG";
    }

    private static Movie[] getPg(Movie[] movies) {
        Movie[] pgMovies=new Movie[3];

        for(int i=0;i< movies.length;i++) {
            if (movies[i].rating.equals("PG")) {
                pgMovies[pgMoviesCount++]=movies[i];

            }
        }

        return pgMovies;
    }



    public static void main(String[] args){
        Movie interstellar=new Movie("Interstellar","RK","R");
        Movie inception = new Movie("Inception","ARKA");

        Movie[] movies=new Movie[3];
        movies[0]=new Movie("Bahubali","Arka Studios","PG");
        movies[1]=new Movie("RRR","Annapurna Studios","R");
        movies[2]=new Movie("Padmavat","Bansali Productions","PG");
        Movie[] pgMovies;
        pgMovies=Movie.getPg(movies);

        System.out.println("PG Movies are:");
        for (int i = 0; i < pgMoviesCount; i++) {
            System.out.println(pgMovies[i].title);
        }

        Movie YJHD =new Movie("Casino Royale","Eon Productions","PG-13");
    }


}
